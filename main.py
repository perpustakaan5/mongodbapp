#Pekan 3 Hari 3

from models import customers
from bson.objectid import ObjectId


#daftar fungsi
def showUsers():
    for doc in customers.objects:
        print(doc.username, doc.fullname, doc.email)


def showUserById(**params):
    for userid in params.values():
        for doc in customers.objects:
            if doc.id == userid:
                print(doc.username, doc.fullname, doc.email)


def insertUser(**params):
    customers(**params).save()


def updateUserById(**params):
    for entry in params.values():
        for doc in customers.objects:
            if doc.id == entry['_id']:
                for key in entry.keys():
                    if key == 'username':
                        doc.username = entry['username']
                    elif key == 'fullname':
                        doc.fullname = entry['fullname']
                    elif key == 'email':
                        doc.email = entry['email']
                    doc.save()


def deleteUserById(**params):
    for userid in params.values():
        for doc in customers.objects:
            if doc.id == userid:
                doc.delete()


#panggil fungsi
if __name__ == "__main__":


    #menampilkan semua data
    showUsers()
    print("seluruh data berhasil ditampilkan")


    # #menampilkan data yang diminta saja
    print('\n')
    print('-'*60)
    dataminta = {'1': 2, '2': 3}
    showUserById(**dataminta)


    #menambah data
    print('\n')
    print('-' * 60)
    datatambah = {
        "username" : 'Mary',
        "fullname" : 'Mary Marijoa',
        "email" : 'mary@gmail.com'
    }
    insertUser(**datatambah)
    showUsers()
    print("data berhasil ditambah")


    # mengubah data
    print('\n')
    print('-' * 60)
    dataubah = {
        '1': {'_id' : ObjectId("606e37ef4a3eb29a63100a4a"),
        'username' : 'MM'}
    }
    updateUserById(**dataubah)
    showUsers()
    print("data berhasil diubah")


    #menghapus data
    print('\n')
    print('-' * 60)
    datahapus = {
        '1': ObjectId("606e3ce9fe57ec427397822b"),
        '2': 4
    }
    deleteUserById(**datahapus)
    showUsers()
    print("data berhasil dihapus")