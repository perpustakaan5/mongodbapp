#Pekan 3 Hari 3

from mongoengine import connect
from mongoengine import Document, StringField

connection = connect(db="rentalfilm", host="localhost", port=27017)

if connection:
    print("MongoDB Connected")


class customers(Document):
    username = StringField(required=True, max_length=70)
    fullname = StringField(required=True, max_length=20)
    email = StringField(required=True, max_length=20)
